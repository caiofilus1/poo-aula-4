//
// Created by caiof on 04/08/2020.
//

#ifndef AULA4_COMPLEX_H
#define AULA4_COMPLEX_H

typedef struct polar_t {
    float module;
    float angule;
} Polar;

class Complex {
public:
    Complex(float real, float imaginary);
    Complex();

    ~Complex();

    Complex operator+(Complex &) ;

    Complex operator-(Complex &) ;

    Complex operator*(Complex &) ;

    Complex operator/(Complex &) ;

    float getReal();

    float getImaginary();

    void printRect();

    void printPolar();

    Polar toPolar();

private:
    float real;
    float imaginary;


};

#endif //AULA4_COMPLEX_H
