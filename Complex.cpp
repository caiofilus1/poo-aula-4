//
// Created by caiof on 04/08/2020.
//
#include "Complex.h"
#include <math.h>
#include <iostream>

using namespace std;

Complex::Complex(float real, float imaginary) {
    this->real = real;
    this->imaginary = imaginary;
}

Complex::Complex() {
    cout << "Digite a parte Real" << endl;
    cin >> this->real;
    cout << "Digite a parte Imaginaria" << endl;
    cin >> this->imaginary;
}

Complex::~Complex() {
}

Complex Complex::operator+(Complex &other) {
    return {this->real + other.getReal(), this->imaginary + other.getImaginary()};
}

Complex Complex::operator-(Complex &other) {
    return {this->real - other.getReal(), this->imaginary - other.getImaginary()};
}

Complex Complex::operator*(Complex &other) {
    Polar thisPolar = this->toPolar();
    Polar otherPolar = other.toPolar();
    float module = thisPolar.module * otherPolar.module;
    float angle = thisPolar.angule = otherPolar.angule;
    return {(float) module * cos(angle), (float) module * sin(angle)};
}

Complex Complex::operator/(Complex &other) {
    Polar thisPolar = this->toPolar();
    Polar otherPolar = other.toPolar();
    float module = thisPolar.module / otherPolar.module;
    float angle = thisPolar.angule - otherPolar.angule;
    return {(float) module * cos(angle), (float) module * sin(angle)};
}

void Complex::printPolar() {
    Polar thisPolar = this->toPolar();
    cout << thisPolar.module << "|" << (thisPolar.angule) * (180 / 3.141592);
}

void Complex::printRect() {
    cout << this->real << " + " << this->imaginary << "j";
}

Polar Complex::toPolar() {
    return {(float) sqrt((double) pow(this->real, 2) + pow(this->imaginary, 2)),
            (float) atan((double) (this->imaginary / this->real))
    };
}

float Complex::getImaginary() {
    return this->imaginary;
}

float Complex::getReal() {
    return this->imaginary;
}