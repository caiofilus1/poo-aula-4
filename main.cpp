#include <iostream>
#include "Complex.h"

int main() {
    std::cout << "Digite o primeiro numero complexo" << std::endl;
    Complex complex1 = Complex();

    complex1.printRect();
    std::cout << " = ";
    complex1.printPolar();
    std::cout << std::endl;

    std::cout << "Digite o primeiro segundo complexo" << std::endl;
    Complex complex2 = Complex();


    complex2.printRect();
    std::cout << " = ";
    complex2.printPolar();
    std::cout << std::endl;


    complex1.printRect();
    std::cout << " + ";
    complex2.printRect();
    std::cout << " = ";
    (complex1 + complex2).printRect();
    std::cout << " = ";
    (complex1 = complex2).printPolar();
    std::cout << std::endl;

    complex1.printRect();
    std::cout << " - ";
    complex2.printRect();
    std::cout << " = ";
    (complex1 - complex2).printRect();
    std::cout << " = ";
    (complex1 - complex2).printPolar();
    std::cout << std::endl;

    complex1.printRect();
    std::cout << " * ";
    complex2.printRect();
    std::cout << " = ";
    (complex1 * complex2).printRect();
    std::cout << " = ";
    (complex1 * complex2).printPolar();
    std::cout << std::endl;

    complex1.printRect();
    std::cout << " / ";
    complex2.printRect();
    std::cout << " = ";
    (complex1 / complex2).printRect();
    std::cout << " = ";
    (complex1 / complex2).printPolar();
    std::cout << std::endl;

    Complex complex3 = complex1 + complex2;
    complex3.printRect();
    complex3.printPolar();


    return 0;
}
